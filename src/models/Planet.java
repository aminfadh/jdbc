/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author Amin
 */
public class Planet {

    private int id;
    private float diameter;
    private String name;
    private boolean hasOwnSatelit;

    public Planet(int id,String name, float diameter, boolean hasOwnSatelit) {
        this.id = id;
        this.diameter = diameter;
        this.name = name;
        this.hasOwnSatelit = hasOwnSatelit;
    }

    public Planet() {
    }

    @Override
    public String toString() {
        return "Planet{" + "id=" + id + ", diameter=" + diameter + ", name=" + name + ", hasOwnSatelit=" + hasOwnSatelit + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getDiameter() {
        return diameter;
    }

    public void setDiameter(float diameter) {
        this.diameter = diameter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasOwnSatelit() {
        return hasOwnSatelit;
    }

    public void setHasOwnSatelit(boolean hasOwnSatelit) {
        this.hasOwnSatelit = hasOwnSatelit;
    }

}
