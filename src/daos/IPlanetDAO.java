/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package daos;

import java.util.List;
import models.Planet;

/**
 *
 * @author Amin
 */
public interface IPlanetDAO {

    public Planet getById(int id);

    public List<Planet> getAll();
    //SELECT * FROM REGIONS WHERE region_name like "%keyword%"

    public List<Planet> search(String keyword);
    //return null jika gagal.

    public Planet insert(Planet p);
    //return null jika gagal.

    public Planet update(Planet p);

    public void delete(int id);
}
