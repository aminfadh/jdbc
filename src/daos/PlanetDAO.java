/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import models.Planet;

/**
 *
 * @author Amin
 */
public class PlanetDAO implements IPlanetDAO {

    private final Connection connection;

    public PlanetDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Planet getById(int id) {
        Planet p = null;
        try {
            String query = "SELECT * FROM GALAXY.PLANET WHERE planet_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int planetId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                float diameter = resultSet.getFloat(3);
                boolean hasOwnSatellite = resultSet.getBoolean(4);
                p = new Planet(planetId, name, diameter, hasOwnSatellite);
                return p;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;
    }

    @Override
    public List<Planet> getAll() {
        List<Planet> planets = new ArrayList<>();
        String query = "SELECT * FROM GALAXY.PLANET ORDER BY planet_id";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int planetId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                float diameter = resultSet.getFloat(3);
                boolean hasOwnSatellite = resultSet.getBoolean(4);
                Planet planet = new Planet(planetId, name, diameter, hasOwnSatellite);
                planets.add(planet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return planets;
    }

    @Override
    public List<Planet> search(String keyword) {
        List<Planet> planets = new ArrayList<>();

        try {
            String query = "SELECT * FROM GALAXY.PLANET WHERE planet_name LIKE initcap(?) OR planet_name LIKE ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, keyword + "%");
            statement.setString(2, "%" + keyword + "%");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int planetId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                float diameter = resultSet.getFloat(3);
                boolean hasOwnSatellite = resultSet.getBoolean(4);
                Planet planet = new Planet(planetId, name, diameter, hasOwnSatellite);
                planets.add(planet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return planets;
    }

    @Override
    public Planet insert(Planet p) {
        try {
            String query = "INSERT INTO GALAXY.PLANET (planet_id, planet_name, diameter, satelit) VALUES (?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, p.getId());
            statement.setString(2, p.getName());
            statement.setFloat(3, p.getDiameter());
            statement.setBoolean(4, p.isHasOwnSatelit());
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            p = null;
        }
        return p;

    }

    @Override
    public Planet update(Planet p) {
        try {
            String query = "UPDATE GALAXY.PLANET SET planet_name = ?, diameter = ?,satelit = ? WHERE planet_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, p.getName());
            statement.setFloat(2, p.getDiameter());
            statement.setBoolean(3, p.isHasOwnSatelit());
            statement.setInt(4, p.getId());
            statement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            p = null;
        }

        return p;
    }

    @Override
    public void delete(int id) {
        try {
            String query = "DELETE FROM GALAXY.PLANET WHERE planet_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
