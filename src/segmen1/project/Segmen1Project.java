/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package segmen1.project;

import controller.IPlanetController;
import controller.PlanetController;
import daos.IPlanetDAO;
import daos.PlanetDAO;
import java.util.List;
import java.util.Scanner;
import models.Planet;

/**
 *
 * @author Amin
 */
public class Segmen1Project {

    public static void main(String[] args) {
        // Inisialisasi objek-objek yang diperlukan
        DBConnection connection = new DBConnection();
        IPlanetDAO planetDAO = new PlanetDAO(connection.getConnection());
        IPlanetController planetController = new PlanetController(planetDAO);

        // Membaca input dari pengguna menggunakan Scanner
        Scanner scanner = new Scanner(System.in);

        boolean isRunning = true;
        while (isRunning) {
            // Menampilkan menu pilihan
            System.out.println("\u001B[32mWelcome to Planet Data Management System\u001B[0m");
            System.out.println("\u001B[36m1. Print all planets\u001B[0m");
            System.out.println("\u001B[36m2. Insert new planet\u001B[0m");
            System.out.println("\u001B[36m3. Update planet data by ID\u001B[0m");
            System.out.println("\u001B[36m4. Search Planet\u001B[0m");
            System.out.println("\u001B[36m5. Delete planet by ID\u001B[0m");
            System.out.println("\u001B[36m6. Exit\u001B[0m");

            // Meminta pengguna untuk memilih menu
            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine(); // Membaca karakter baru setelah angka
            switch (choice) {
                case 1:
                    // Menampilkan semua planet
                    printAll(planetController);
                    break;
                case 2:
                    // Meminta pengguna memasukkan data baru
                    System.out.print("Enter planet ID: ");
                    String id = scanner.nextLine();
                    System.out.print("Enter planet name: ");
                    String name = scanner.nextLine();
                    System.out.print("Enter planet diameter: ");
                    String diameter = scanner.nextLine();
                    System.out.print("Enter hasOwnSatellite (true/false): ");
                    String hasOwnSatellite = scanner.nextLine();

                    // Memanggil metode testInsert untuk memasukkan data baru
                    testInsert(planetController, id, name, diameter, hasOwnSatellite);
                    break;
                case 3:
                    // Meminta pengguna memasukkan ID planet yang ingin diperbarui
                    System.out.print("Enter planet ID to update: ");
                    String idToUpdate = scanner.nextLine();

                    // Memanggil metode testUpdate untuk memperbarui data planet
                    testUpdate(planetController, idToUpdate, scanner);
                    break;
                case 4:
                    // Meminta pengguna memasukkan kata kunci pencarian
                    System.out.print("Enter search keyword: ");
                    String keyword = scanner.nextLine();

                    // Memanggil metode testSearch untuk mencari planet berdasarkan kata kunci
                    testSearch(planetController, keyword);
                    break;

                case 5:
                    // Meminta pengguna memasukkan ID planet yang ingin dihapus
                    System.out.print("Enter planet ID to delete: ");
                    String idToDelete = scanner.nextLine();

                    // Memanggil metode testDelete untuk menghapus data planet
                    testDelete(planetController, idToDelete);
                    break;
                case 6:
                    // Keluar dari program
                    System.out.println("Exiting the program...");
                    isRunning = false;
                    break;

                default:
                    System.out.println("Invalid choice!");
                    break;
            }
        }
    }

    // Metode testInsert untuk memasukkan data planet baru
    public static void testInsert(IPlanetController planetController, String id, String name, String diameter, String hasOwnSatellite) {
        String insertResult = planetController.insert(id, name, diameter, hasOwnSatellite);
        System.out.println("Insert Result: " + insertResult);
        printAll(planetController);
    }

    // Metode testUpdate untuk memperbarui data planet
    public static void testUpdate(IPlanetController planetController, String id, Scanner scanner) {
        List<Planet> planetById = planetController.getById(id);

        if (!planetById.isEmpty()) {
            Planet planet = planetById.get(0);

            // Menampilkan data planet yang akan diperbarui
            System.out.println("Current Planet Data:");
            System.out.println("ID: " + planet.getId());
            System.out.println("Name: " + planet.getName());
            System.out.println("Diameter: " + planet.getDiameter());
            System.out.println("Has Own Satellite: " + planet.isHasOwnSatelit());

            // Meminta pengguna memasukkan data baru
            System.out.print("Enter new name: ");
            String newName = scanner.nextLine();
            System.out.print("Enter new diameter: ");
            String newDiameter = scanner.nextLine();
            System.out.print("Enter new hasOwnSatellite (true/false): ");
            String newHasOwnSatellite = scanner.nextLine();

            // Memanggil metode update dengan data baru
            String updateResult = planetController.update(id, newName, newDiameter, newHasOwnSatellite);
            System.out.println("Update Result: " + updateResult);
            printAll(planetController);
        } else {
            System.out.println("Planet with ID " + id + " not found.");
        }
    }

    public static void testSearch(IPlanetController planetController, String keyword) {
        List<Planet> searchedPlanets = planetController.search(keyword);

        if (!searchedPlanets.isEmpty()) {
            System.out.println("Searched Planets:");
            System.out.println("-------------------------------------------------------");
            System.out.printf("%-10s | %-15s | %-10s | %-10s%n", "ID", "Name", "Diameter", "Has Own Satellite");
            System.out.println("-------------------------------------------------------");
            searchedPlanets.forEach(planet -> {
                System.out.printf("%-10s | %-15s | %-10s | %-10s%n", planet.getId(), planet.getName(), planet.getDiameter(), planet.isHasOwnSatelit());
            });
            System.out.println("-------------------------------------------------------");
        } else {
            System.out.println("No planets found with the given keyword.");
        }
    }

    // Metode testDelete untuk menghapus data planet
    public static void testDelete(IPlanetController planetController, String id) {
        String deleteResult = planetController.delete(id);
        System.out.println("Delete Result: " + deleteResult);
        printAll(planetController);
    }

    // Metode printAll untuk menampilkan semua planet
    public static void printAll(IPlanetController planetController) {
        List<Planet> allPlanets = planetController.getAll();

        System.out.println("All Planets:");
        System.out.println("-------------------------------------------------------");
        System.out.printf("%-10s | %-15s | %-10s | %-10s%n", "ID", "Name", "Diameter", "Has Own Satellite");
        System.out.println("-------------------------------------------------------");
        allPlanets.forEach(planet -> {
            System.out.printf("%-10s | %-15s | %-10s | %-10s%n", planet.getId(), planet.getName(), planet.getDiameter(), planet.isHasOwnSatelit());
        });
        System.out.println("-------------------------------------------------------");

    }
}
