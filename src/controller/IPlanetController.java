/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package controller;

import java.util.List;
import models.Planet;

/**
 *
 * @author Amin
 */
public interface IPlanetController {

    public String insert(String id, String name,String diameter, String hasOwnSatellite);

    public List<Planet> getAll();

    public List<Planet> getById(String id);

    public List<Planet> search(String key);

    public String update(String id, String name, String diameter, String hasOwnSatellite);

    public String delete(String id);
}
