/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import daos.IPlanetDAO;
import java.util.ArrayList;
import java.util.List;
import models.Planet;

/**
 *
 * @author Amin
 */
public class PlanetController implements IPlanetController {

    private IPlanetDAO planetDAO;

    public PlanetController(IPlanetDAO planetDAO) {
        this.planetDAO = planetDAO;
    }

    @Override
    public String insert(String id, String name, String diameter, String hasOwnSatellite) {
        int planetId;
        float planetDiameter;
        boolean planetHasOwnSatellite;

        try {
            planetId = Integer.parseInt(id);
            planetDiameter = Float.parseFloat(diameter);
            planetHasOwnSatellite = Boolean.parseBoolean(hasOwnSatellite);
        } catch (Exception e) {
            return "Invalid input format";
        }

        Planet planet = new Planet(planetId, name, planetDiameter, planetHasOwnSatellite);

        Planet insertedPlanet = planetDAO.insert(planet);
        if (insertedPlanet != null) {
            return "Planet inserted successfully";
        } else {
            return "Failed to insert planet";
        }
    }

    @Override
    public List<Planet> getAll() {
        return planetDAO.getAll();
    }

    @Override
    public List<Planet> getById(String id) {
        int planetId;
        try {
            planetId = Integer.parseInt(id);
        } catch (Exception e) {
            return null;
        }

        Planet planet = planetDAO.getById(planetId);
        if (planet != null) {
            List<Planet> planetList = new ArrayList<>();
            planetList.add(planet);
            return planetList;
        } else {
            return null;
        }
    }

    @Override
    public List<Planet> search(String key) {
       List<Planet> planet = planetDAO.search(key);
        if (planet != null) {
            return planet;
        } else {
            return null;
        }
    }

    @Override
    public String update(String id, String name, String diameter, String hasOwnSatellite) {
        int planetId;
        float planetDiameter;
        boolean planetHasOwnSatellite;

        try {
            planetId = Integer.parseInt(id);
            planetDiameter = Float.parseFloat(diameter);
            planetHasOwnSatellite = Boolean.parseBoolean(hasOwnSatellite);
        } catch (Exception e) {
            return "Invalid input format";
        }

        Planet planet = new Planet(planetId, name, planetDiameter, planetHasOwnSatellite);

        Planet updatedPlanet = planetDAO.update(planet);
        if (updatedPlanet != null) {
            return "Planet updated successfully";
        } else {
            return "Failed to update planet";
        }
    }

    @Override
    public String delete(String id) {
        int planetId;
        try {
            planetId = Integer.parseInt(id);
        } catch (Exception e) {
            return "Invalid planet ID";
        }

        planetDAO.delete(planetId);
        return "Planet deleted successfully";
    }
}
